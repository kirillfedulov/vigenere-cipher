#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ALPHABET_LENGTH 26

void usage(const char *program) {
    printf("Usage: %s [encrypt|decrypt] message password\n", program);
    exit(1);
}

void encrypt(const char *message, const char *password) {
    int message_length  = strlen(message),
        password_length = strlen(password);
    int i = 0, j = 0;
    while (i < message_length) { 
        int c = message[i];
        if (isalpha(c)) {
            int k = password[j] - 'a';
            j = (j + 1) % password_length;
            int shift = (c - 'a' + k) % ALPHABET_LENGTH;
            c = 'a' + shift;
        }
        putchar(c);
        i++;
    }
    putchar('\n');
}

void decrypt(const char *message, const char *password) {
    int message_length  = strlen(message),
        password_length = strlen(password);
    int i = 0, j = 0;
    while (i < message_length) {
        int c = message[i];
        if (isalpha(c)) {
            int k = password[j] - 'a';
            j = (j + 1) % password_length;
            int shift = (c - 'a' + ALPHABET_LENGTH - k) % ALPHABET_LENGTH;
            c = 'a' + shift;
        }
        putchar(c);
        i++;
    }
    putchar('\n');
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        usage(argv[0]);
    }
    const char *action = argv[1];
    if (strcmp(action, "encrypt") == 0) {
        encrypt(argv[2], argv[3]);
    } else if (strcmp(action, "decrypt") == 0) {
        decrypt(argv[2], argv[3]);
    } else {
        usage(argv[0]);
    }
}
